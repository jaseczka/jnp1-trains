/*
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * JNP1 / Zadanie 1
 * Nazwa: pociagi.cc
 * Autorzy: Magdalena Teperowska
 *          Magdalena Zaremba
 * Data: 17.10.2013
 *
 * Przydatne flagi kompilacji: -std=c++11 -lboost_regex
 *
 * Przyjęte założenia warte wzmiany: 
 *  1. Numer pociągu składa się z 3-9 cyfr.
 *  2. Zakres dozwolonych dat przyjazdu: lata 1800 - 9999.
 *  3. Wartość opóźnienia nie może przekraczać
 *      wartości pozostałej do północy.
 *      (Skoro Tp < Tk, czyli mamy nie obsługiwać "przeskoku"
 *      przez północ, to stwierdziłyśmy, że również
 *      będziemy obsługiwać "przeskoku" dla opóźnień).
 *  4. Nie przetwarzamy linii dłuższych niż 10000 znaków.
 *  5. Jeśli linia jest zbyt długa, być może nie zostanie
 *      wypisana na cerr w zniezmienionej postaci.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */
  
#include <iostream>
#include <algorithm>
#include <vector>
#include <utility>
#include <cctype>
#include <string>
#include <boost/regex.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>

typedef std::pair<int, int> train_data;
typedef std::vector<train_data> train_collection;

const unsigned short MAX_HOUR = 23;
const unsigned short MIN_HOUR = 0;
const unsigned short MIN_MIN = 0;
const unsigned short MAX_MIN = 59;
const int MIN_YEAR = 1800;
const int MAX_YEAR = 9999;
const int MIN_MONTH = 1;
const int MAX_MONTH = 12;
const char L_COMMAND = 'L'; 
const char M_COMMAND = 'M';
const char S_COMMAND = 'S';
const int HOURS_PER_DAY = 24;
const int MINS_PER_HOUR = 60;
const int MINS_PER_DAY = HOURS_PER_DAY * MINS_PER_HOUR;
const int MAX_LINE_LEN= 10000;   

/** Sprawdza czy zadany dzień, miesiąc, rok
 *  tworzą poprawną datę.
 */
bool checkDate(int day, int month, int year){

    if (year < MIN_YEAR || year > MAX_YEAR || 
        month < MIN_MONTH || month > MAX_MONTH ||
        day < 1 || 
        day > boost::gregorian::gregorian_calendar::end_of_month_day(year, month))
        return false;
    
    return true;
}

/** Sprawdza czy zadana godzina i minuta tworzą poprawny
 *  format czasu.
 */
bool checkHour(int hours, int minutes) {
    if (hours > MAX_HOUR || hours < MIN_HOUR || 
        minutes > MAX_MIN || minutes < MIN_MIN)
       return false;
    return true;
}

/** Wypisuje błędną linię na strumień błędów,
 *  lineNumber - numer błędnego wiersza.
 */
void writeError(const std::string &line, int lineNumber) {
    std::cerr << "Error " << lineNumber << ": " <<  line << "\n";
}

/** Sprawdza poprawność wartości opóźnienia.
 *  Zakładamy, że nie przetwarzamy pociągów, 
 *  których opóźnienie powoduje przyjazd po północy.
 */
bool checkDelay(int hours, int minutes, int delay) {
    if (delay <= 0 || delay >= MINS_PER_DAY)
        return false;
    if (hours * MINS_PER_HOUR + minutes + delay >= MINS_PER_DAY)
        return false;
    return true;
}

/** Liczy minuty od północy na podstawie liczby godzin i minut.
 */
int makeTime(int hours, int minutes, int delay = 0) {
    return hours * MINS_PER_HOUR + minutes + delay;
}

/** Sprawdza czy dana linia line reprezentuje 
 *  poprawne dane o pociągu.
 */
bool checkIfTrain(const std::string &line, train_collection &trains) {
    if (line.size() > MAX_LINE_LEN)
       return false;
    int arrival_time = 0;
    int delay = 0;

    static const boost::regex line_regex("\\s*\\d{3,9}\\s+(\\d{1,2})\\.(\\d{1,2})\\.(\\d{4})\\s+(\\d{1,2})\\.(\\d{2})(?:\\s+(\\d{1,4}))?\\s*");
    boost::cmatch matches;
    if (!boost::regex_match (line.c_str(), matches, line_regex)) {
        return false;
    }

    int day = boost::lexical_cast<int>(matches[1]);
    int month = boost::lexical_cast<int>(matches[2]);
    int year = boost::lexical_cast<int>(matches[3]);
    
    if (!checkDate(day, month, year)) 
        return false; 

    int hours = boost::lexical_cast<int>(matches[4]);
    int minutes = boost::lexical_cast<int>(matches[5]);
    
    if (!checkHour(hours, minutes)) 
        return false;
    
    if (matches[6].length() > 0) {
        delay = boost::lexical_cast<int>(matches[6]);
    
        if (!checkDelay(hours, minutes, delay))
            return false;
    }
    
    arrival_time = makeTime(hours, minutes, delay);

    trains.push_back(std::make_pair(arrival_time, delay));
    return true;
}

/** Sprawdza czy dana linia line reprezentuje
 *  poprawne polecenie. Podstawia odpowiednie parametry
 *  w miejsce commandType, startTime, endTime
 *  (typ polecenia, Tp, Tk)
 */
bool checkIfCommand(const std::string &line, 
char &commandType, int &startTime, int &endTime) {
    static const boost::regex line_regex("^\\s*([LMS])\\s+(\\d{1,2}).(\\d{2})\\s+(\\d{1,2}).(\\d{2})\\s*$");
    boost::cmatch matches;
    
    if (!boost::regex_match (line.c_str(), matches, line_regex)) {
        return false;
    }
    
    commandType = boost::lexical_cast<char>(matches[1]);
    int startHour = boost::lexical_cast<int>(matches[2]);
    int startMinutes = boost::lexical_cast<int>(matches[3]);
    int endHour = boost::lexical_cast<int>(matches[4]);
    int endMinutes = boost::lexical_cast<int>(matches[5]);
    
    if (!checkHour(startHour, startMinutes)) 
        return false;
    if (!checkHour(endHour, endMinutes)) 
        return false;
    
    startTime = makeTime(startHour, startMinutes);
    endTime = makeTime(endHour, endMinutes);
    
    if (endTime < startTime)
        return false;
    
    return true;
}

/** Komparator do sortowania pociągów i wyszukiwania binarnego.
 */
bool trainsCompare(train_data first, train_data second) {
    return first.first < second.first;
}

/** Znajduje parę iteratorów wskazującą na początek
 *  i koniec przedziałów czasowych (Tp i Tk) w wektrze
 *  pociągów posortowanych po czasie przyjazdu.
 */
std::pair<train_collection::iterator, train_collection::iterator> findBounds(
train_collection &trains, int start, int end) {
    train_collection::iterator first, last;
    first = std::lower_bound(trains.begin(), trains.end(), 
        std::make_pair(start, 0), trainsCompare);
    last = std::upper_bound(trains.begin(), trains.end(), 
        std::make_pair(end, 0), trainsCompare);
    return make_pair(first, last);
}

/** Wypisuje wynik polecenia L na zadanym przedziale czasu.
 *  Zakłada poprawność danych wejściowych.
 */
void writeL(train_collection &trains, int start, int end) {
    std::pair<train_collection::iterator, train_collection::iterator> bounds = 
        findBounds(trains, start, end);
    std::cout << bounds.second - bounds.first << "\n";
}

/** Wypisuje wynik polecenia M na zadanym przedziale czasu.
 *  Zakłada poprawność danych wejściowych.
 */
void writeM(train_collection &trains, int start, int end) {
    std::pair<train_collection::iterator, train_collection::iterator> bounds = 
        findBounds(trains, start, end);
    int maxDelay = 0;
    for(train_collection::iterator it = bounds.first; it != bounds.second; ++it) {
        if (it->second > maxDelay) 
            maxDelay = it->second;
    }
    std::cout << maxDelay << "\n";
}

/** Wypisuje wynik polecenia S na zadanym przedziale czasu.
 *  Zakłada poprawność danych wejściowych.
 */
void writeS(train_collection &trains, int start, int end) {
    std::pair<train_collection::iterator, train_collection::iterator> bounds = 
    findBounds(trains, start, end);
    int sumDelay = 0;
    for(train_collection::iterator it = bounds.first; it != bounds.second; ++it)
        sumDelay += it->second;
    std::cout << sumDelay << "\n";
}

/** Wypisuje wynik polecenia commandType na zadanym przedziale czasu.
 *  Zakłada poprawność danych wejściowych
 */
void executeCommand(train_collection &trains, 
char commandType, int start, int end) {
    switch(commandType) {
        case L_COMMAND: 
            writeL(trains, start, end);
            break;
        case M_COMMAND:
            writeM(trains, start, end);
            break;
        case S_COMMAND:
            writeS(trains, start, end);
            break;
    }
}

/** Przygotowuje wektor pociągów do przetwarzania. (sortuje)
 */
void prepareTrains(train_collection &trains) {
    std::sort(trains.begin(), trains.end(), trainsCompare);
}

int main() {
    bool shouldBeTrain = true;
    train_collection trains;
    int lineNumber = 0;
    std::string line;
    char commandType;
    int commandStartHour, commandEndHour;
    while (std::getline(std::cin, line)) {
        ++lineNumber;
        if (line.size() > MAX_LINE_LEN) {
            writeError(line, lineNumber);
        } else if (shouldBeTrain && checkIfTrain(line, trains)) {
            ;
        } else if (checkIfCommand(line, commandType, 
            commandStartHour, commandEndHour)) {
            if (shouldBeTrain) {
                prepareTrains(trains);
                shouldBeTrain = false;
            }
            executeCommand(trains, commandType, 
                commandStartHour, commandEndHour);
        } else {
            writeError(line, lineNumber); 
        }
    }

    return 0;
}
